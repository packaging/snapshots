#!/bin/sh

: "${ICINGA_TOKEN:=}"
: "${GITHUB_TOKEN:=}"

if [ -z "${ICINGA_TOKEN}" ]; then
  echo "Environment var ICINGA_TOKEN is not set!" >&1
  exit 1
fi
if [ -z "${GITHUB_TOKEN}" ]; then
  echo "Environment var GITHUB_TOKEN is not set!" >&1
  exit 1
fi

export ICINGA_TOKEN GITHUB_TOKEN

echo "Replacing vars from icinga_snapshotter.yml to icinga_snapshotter.secrets.yml"
envsubst < icinga_snapshotter.yml > icinga_snapshotter.secrets.yml
