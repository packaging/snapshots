Icinga Snapshots
================

This repository configures which problems are automatically build as snapshots.

Configuration is stored in [icinga_snapshotter.yml](icinga_snapshotter.yml)

[icinga-snapshotter](https://git.icinga.com/testing/icinga-snapshotter) is a small Python tool. It checks GitHub's API for the latest commit of a project and triggers a new GitLab CI build in the target repository, if there was not yet a build with this commit hash.

All builds are triggered with the [Icinga Snapshots](https://git.icinga.com/icinga-snapshots) user in GitLab.
